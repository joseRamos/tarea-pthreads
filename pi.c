//Para compilar: gcc pi.c -o pi
//Para correr: ./pi

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define SEED time(NULL)


//Se utiliza el metodo de Monte Carlo
int main() {

 srand( SEED );
 int i, count;
 int n = 10000000; //Esto para que tenga el mismo valor de iteraciones que en mutex
 double x,y,z,pi;

//Se ingresa el valor de iteraciones
//Esto era por si el usuario quiere ingresar el numero de iteraciones
// printf("n = ");
// scanf("%d", &n);

 count = 0;

 for(i = 0; i < n; ++i) {
//Aplicamos el metodo
     x = (double)rand() / RAND_MAX;

     y = (double)rand() / RAND_MAX;

     z = x * x + y * y;

     if( z <= 1 ) count++;
 }
//Se calcula el valor de Pi
 pi = (double) count / n * 4;

 printf("\n --------- Aproximacion de Pi en serial: -----------\n\n");

 printf("El valor aproximado de Pi es = %g", pi);

 return 0;
}
