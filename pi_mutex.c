#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#define NR_PTS 10000000
#define NR_THREADS 5
#define NR_PTRS_PER_THREAD NR_PTS/NR_THREADS

//Para el manejo de errores en pthread_create y en pthread_join
#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

//Se utiliza el metodo de Monte Carlo
long circleCount =0; //Contador de Circulos Global
pthread_t *threads; //Declaracion del puntero del thread
pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER; //Inicializamos el mutex

void *monteCarloPi(void *thread_ID) {
  long i; //Contador
  int a = (int) thread_ID; //Thread para numeros
  long incircle_thread=0; //Numero de puntos en  el thread de este Circulo
  unsigned int rand_state = rand(); // Random Generator


	for (i = 0; i < NR_PTRS_PER_THREAD; i++) {


    double x = rand_r(&rand_state) / ((double)RAND_MAX + 1) * 2.0 - 1.0;
		double y = rand_r(&rand_state) / ((double)RAND_MAX + 1) * 2.0 - 1.0;

		// Si el punto esta dentro del circulo
		if (x * x + y * y < 1) {

			//Aumentamos el contador del numero de incircle
			incircle_thread++;

}
}
//Calculamos la aproximacion de pi para el thread
  float Pi = (4. * (double)incircle_thread) /
				((double)NR_PTRS_PER_THREAD * 1);
  //Imprimimos
    printf("El Thread [%d] dice que Pi es:  [%f]\n" ,a,Pi);
//Lockeamos thread
    pthread_mutex_lock(&mutex);

    		circleCount += incircle_thread;
//Unlockeamos thread
    		pthread_mutex_unlock(&mutex);

    return NULL;

    }

    void createThreads(){

    	int i, s;

    	threads = malloc(NR_THREADS * sizeof(pthread_t)); //Le asignamos espacio al thread

    	pthread_attr_t attr; //Init del atributo del thread

    	pthread_attr_init(&attr);

    	printf("\n----------------------------------------\n*Creando [%d] Threads\n\n", NR_THREADS);
    	// Crea 1 thread por cada NR_THREADS */
    	for (i = 0; i < NR_THREADS; i++) {

    		s = pthread_create(&threads[i], &attr, monteCarloPi,  (void *) i);
    	 	 //Si se recibe algo distino a 0, hay un create error*/
    		 if (s != 0){

    			handle_error_en(s, "pthread_create");

    		 }
    	}

    }

 void joinThreads(){

	int i,s;

	//Unimos 1 thread por cada NR_THREADS */
	for (i = 0; i < NR_THREADS; i++) {

		s = pthread_join(threads[i], NULL);
		 //Si se recibe algo distino a 0, hay un join error*/
		 if (s != 0){

			handle_error_en(s, "pthread_join");

		 }

	}

	// Destruimos el mutex
	pthread_mutex_destroy(&mutex);


	printf("\n*[%d] Threads Reunidos\n\n", NR_THREADS);

	//Liberamos el espacio de los threads
	free(threads);
}


int main() {

        printf("\n --------- Aproximacion de Pi en paralelo: ----------- \n");

	float Pi;

	createThreads();

	joinThreads();

	//Calculo de PI con Monte Carlo
	Pi = (4. * (double)circleCount) / ((double)NR_PTRS_PER_THREAD * NR_THREADS);

	//Mensaje final de la aproximacion
	printf("Estimacion final de Pi: %f\n\n----------------------------------------\n", Pi);

return 0;

}
