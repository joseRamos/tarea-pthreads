#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define MAXTHREADS 1000000

//Inicializacion de variables globales
int pass=1;
int *vector;
int *resultado;
int *comparar;
int **matriz;
int n;


//funciones
void* mul(int* arg) {
  for(int i = 0; i < n; i++)
  {
    resultado[*arg] += vector[i]*matriz[i][*arg];
  }
}

void print()
{
    printf("[ ");
    for (int i = 0 ; i < n; i++)
    {
      printf("%i ",vector[i]);
    }
    printf("]\n X \n");
    for (int i = 0; i < n; i++)
    {
      printf("| ");
      for (int k = 0; k < n; k++)
      {
        printf("%i ",matriz[i][k]);
      }
      printf("|\n");
    }

    printf(" = \n[ ");
    for (int i = 0 ; i < n ; i++)
    {
      printf("%i ",resultado[i]);
    }
    printf("]\n");
}

int main(int argc, char *argv[])
{
    //Inicializacion de variables locales
    pthread_t pid[MAXTHREADS];
    pthread_attr_t attrs;
    time_t t;
    int cnt;
    srand((unsigned) time(&t));
    n = (rand()%5)+3;
    int *indice;
    clock_t inicio;
    clock_t fin;

    //Reservar memoria
    vector = malloc(n * sizeof(int));
    comparar = malloc(n * sizeof(int));
    resultado = malloc(n * sizeof(int));
    indice = malloc(n * sizeof(int));
    matriz = (int **)malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++)
    {
      matriz[i] = (int *)malloc(n * sizeof(int));
    }

    //Cargar valores iniciales
    for (int columna = 0; columna < n; columna++)
    {
      vector[columna] = rand()%10;
      resultado[columna] = 0;
      indice[columna] = columna;
      for (int fila = 0; fila < n; fila++)
      {
        matriz[fila][columna] = rand()%10;
      }
    }

    // ================== PARALELO ===================
    pthread_attr_init(&attrs);

    for (cnt = 0; cnt < n; cnt++) {
        pthread_create(&pid[cnt], &attrs, (void*)mul, &indice[cnt]);
    }

    for (int i = 0; i < cnt; i++)
    {
        pthread_join(pid[i], NULL);
    }

    pthread_attr_destroy(&attrs);

    //Imprimir resultado
    printf("\n ----- Resultado: Implementacion PARALELO: ----- \n\n");
    print();

    // ================== SERIAL ===================

    //Limpiar memoria
    for (int columna = 0; columna < n; columna++)
    {
      comparar[columna]=resultado[columna];
      resultado[columna] = 0;
    }

    for (int i = 0 ; i < n ; i++)
    {
      mul(&indice[i]);
    }

    //Imprimir resultado
    printf("\n ----- Resultado: Implementacion SERIAL: ----- \n\n");
    print();

    //Comparar resultados
    for (int columna = 0; columna < n; columna++)
    {
      if(comparar[columna]!=resultado[columna])
      {
        pass = 0;
      }
    }

    //Liberar memoria
    free(vector);
    free(resultado);
    free(indice);
    for (int i = 0; i < n; i++)
    {
      free(matriz[i]);
    }

    //return
    if(pass==1)
    {
      return 0;
    } else {
      return 1;
    }

}
